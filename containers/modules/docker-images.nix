{ config, lib, ... }:

{
  options = {

    generated.dockerImages = lib.mkOption {
      type = with lib.types; attrsOf package;
      default = {};
      description = ''
        A place to put ALL THE docker images.
      '';
    };

  };
}


