let
  pkgs = import <nixpkgs> {};

in pkgs.lib.fix (a: import ./. a pkgs)
