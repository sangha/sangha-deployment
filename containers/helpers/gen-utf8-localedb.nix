{ pkgs, helpers }:
# Generate a localedb for the given UTF-8 locale string.

# a locale string formatted like `en_US.UTF-8`
locale:

pkgs.glibcLocales.override {
  locales = [ (locale + "/UTF-8") ];
  allLocales = false;
}
