{ pkgs, helpers }:
# Thin, leaky wrapper arount `buildDockerImage`
# that sets up a minimal sane environment for running
# programs without depending on an OS base image (like Alpine).
# All attributes that are named the same as `dockerTools.buildImage`
# attrs work the same.
{
  # : string
  name,
  # : string
  tag,
  # : contents TODO
  contents,
  # : dockerConfig TODO
  config,
  # : lines
  description,
  # like runAsRoot, but the base setup is already done
  rootSetupScript ? "",
  # Add an init process as entry point that reaps zombie processes.
  # See https://github.com/krallin/tini
  # and https://github.com/docker-library/official-images#init
  # Note that `docker run` can do that via `--init`, but that
  # is given at deploy time and therefore easy to forget.
  initProcess ? [ "${pkgs.tini}/bin/tini" "--" ]
}:
let

  # this might bite, but is probably a sane default
  user = config.User or (builtins.replaceStrings [":"] ["-"] name);

  uidGidStr = toString 999;

  # We split up a base image, because the contents
  # are changed less often, which leads docker to
  # reuse that whole layer when the config or the
  # rootSetupScript changes.
  baseImage = pkgs.dockerTools.buildImage {
    name = "${name}-base";
    inherit tag;
    inherit contents;
  };

  image = pkgs.dockerTools.buildImage {
    inherit name tag;
    fromImage = baseImage;

    config = config // {
      # initProcess is always prepended to the given entrypoint
      Entrypoint = initProcess ++ config.Entrypoint or [];
    };

    # TODO: read-only container option
    # TODO: make most setup work completely declarative
    runAsRoot = ''
      ${pkgs.stdenv.shell}
      ${pkgs.dockerTools.shadowSetup}

      ## some needed system structure

      # sane default file permissions
      umask 022

      # TODO: tmp should probably be a tmpfs volume of some kind
      mkdir --mode=777 /tmp

      # the user our program is run as
      echo "${user}::${uidGidStr}:${uidGidStr}::::" > /etc/passwd
      # also gets a group
      echo "${user}:x:${uidGidStr}:${user}" > /etc/group

      # make localhost resolvable
      echo "127.0.0.1 localhost" > /etc/hosts
      # enable hosts file (`files`) and dns resolution (`dns`)
      # TODO: might make sense to make this more generic
      # libnss_dns.so, libnss_files.sh … provide these
      # (TODO where do they come from?)
      echo "hosts: files dns" > /etc/nsswitch.conf

      ''
      + rootSetupScript;
  };

in image.overrideAttrs (_: {
    meta = { inherit description; };
})
