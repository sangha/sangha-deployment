# the functions in this module
helpers:
# nixpkgs
pkgs:
# Lots of small and big helper functions.
# See their source files for docs.
# This is an overlay if called with the following function:
# self: _: { helpers = import ./. self.helpers self; }

let
  call = path: import path { inherit pkgs helpers; };

  # some of the helpers where upstreamed to nixperiments
  nixperiments = import (pkgs.fetchFromGitHub {
    owner = "Profpatsch";
    repo = "nixperiments";
    rev = "b0de707f1e32adf9b29ff16bcaa30b6e08b65fea";
    sha256 = "05498wn2r6j92li5kjv937y54v1yvvm6j0jbqzngzrq74mwy8ay2";
  }) { nixpkgs = pkgs.path; };

in {
  # -- GENERAL  --

  # generate an optparser for a bash script
  script = nixperiments.script;

  # transform json files with nix code (jq but with nix)
  inherit (nixperiments)
    json2json
    json2string;

  # prepend a folder to all files in a derivation output
  prependPrefix = call ./prepend-prefix.nix;


  # -- UPDATER --

  # updater for fetchgit packages
  fetchgitUpdater = call ./fetchgit-updater;


  # -- DOCKER --

  # awesome wrapper around dockerTools
  createStandaloneDockerImage = call ./create-standalone-docker-image.nix;
  # add debugging layer to docker image
  debugDockerImage = call ./debug-docker-image.nix;
  # generate a subset locale db
  genUtf8Localedb = call ./gen-utf8-localedb.nix;


  # -- MISC --

  # generate (and init) postgresql database
  postgresTemplate = call ./postgres-template.nix;
}
