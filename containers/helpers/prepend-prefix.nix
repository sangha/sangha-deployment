{ pkgs, helpers }:
# Prepend folders to a derivations’s contents.

# folder prefix
prefix:
# original derivation
drv:
pkgs.runCommand "${drv.name}-prefixed" {} ''
  new="$out/${prefix}"
  mkdir -p $(dirname "$new")
  cp -r ${drv} "$new"
  # nix-support folder should remain on the top level
  cp -r $new/nix-support $out/
''

