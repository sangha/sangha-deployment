{ pkgs, helpers }:
# Make a generated docker image debuggable,
# by including bash and coreutils.
# `docker run -it <image>`
image:

pkgs.dockerTools.buildImage {
  name = image.imageName;
  tag = image.imageTag;
  fromImage = image;
  contents = [
    pkgs.strace
    pkgs.bash
    pkgs.coreutils
    pkgs.su-exec
  ];

  inherit (image.buildArgs) config;
}
