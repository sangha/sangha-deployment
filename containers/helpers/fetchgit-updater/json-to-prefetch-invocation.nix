{ nixPrefetchGit }:
# Transform a nix-prefetch-git source json file
# to an a nix-prefetch-git invocation that takes
# the correct command line flags.
let
  # Some attributes in the json are named differently
  # than their command flag. Some even don’t map
  # directly (like `deepClone`).
  changedKeys = {
    "deepClone" = b:
      if b then "deepClone" else "no-deepClone";
    "leaveDotGit" = const "leave-dotGit";
    "fetchSubmodules" = const "fetch-submodules";
  };
  # keys that should not be passed
  filteredKeys = [
    # if --sha256 is passed, nix-prefetch-git does nothing
    "sha256"
  ];
  # filter out filteredKeys
  keyFilter = filterAttrs (k: _: all (k': k != k') filteredKeys);
  # transform the keys to their correct flag names
  keyTrans = mapAttrs'
    (k: v: nameValuePair ((changedKeys.${k} or id) k) v);
  # flag invocation
  toFlag = k: v: "--${k}"
    + (if isBool v then "" # boolean flag
      else " ${toString v}"); # option with value
  # the final shell invocation
  # $1 still needs the nix-prefetch-git path
  toInvoc = flags: ''
    ${nixPrefetchGit} \
      ${concatStringsSep " \\\n  " (mapAttrsToList toFlag flags)}
   '';

in
  json: toInvoc (keyTrans (keyFilter json))
