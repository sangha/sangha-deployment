{ pkgs, helpers }:
# Updater for git sources
let
  args = pkgs.writeText "args.nix" ''
    { nixPrefetchGit = "${pkgs.nix-prefetch-scripts}/bin/nix-prefetch-git"; }
  '';

in helpers.script.withOptions {
  name = "fetchgit-updater";
  synopsis = "Update the json description of a git source.";
  options = {
    file = {
      description = "The json file to update.";
      checks = [ helpers.script.optionChecks.fileExists ];
    };
  };
  script = ''
  ${helpers.json2string} \
    ${args} \
    ${./json-to-prefetch-invocation.nix} \
      "$file"
  '';
}
