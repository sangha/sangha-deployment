{ pkgs, helpers }:
# Generates a postgresql template by executing initdb.
# Initialization of the database server (e.g. adding roles
# and databases) can be done by an init script like:
# psql --file=./mocks/projects.sql --username=postgres sangha
{
# template name
# : string
name,
# `genUtf8Localedb`
# : localeDrv
locale,
# script to connect to the database running locally
# : lines
initScript,
# attrset of postgresql.conf values
# : attrs nixScalar
postgresqlConf
}:

let
  localedb = helpers.genUtf8Localedb locale;

  config =
    let
      format = with pkgs.lib.generators; toKeyValue {
        mkKeyValue = mkKeyValueDefault {
          mkValueString = v: if builtins.isString v then "'${v}'" else toString v;
        } "=";
      };
    in format postgresqlConf;

in pkgs.runCommand "postgres-template-${name}" {
    nativeBuildInputs = [ pkgs.postgresql localedb ];
  } ''
    mkdir $out
    export PGDATA=$out

    # init the postgres database in $out
    initdb --nosync --username=postgres \
      --encoding=UTF8 --locale=${locale}

    # overwrite generated config file
    mv $PGDATA/postgresql.conf $PGDATA/postgresql.example.conf
    cp ${pkgs.writeText "sangha-postgresql.conf" config} \
        $PGDATA/postgresql.conf

    # start postgres server in build environment
    pg_ctl start -w

    ${initScript}

    pg_ctl stop -w
''
