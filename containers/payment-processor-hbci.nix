{ lib, pkgs, config, ... }:
let
  cfg = config.org.techcultivation.deploy.paymentProcessorHbci;
in {

  options.org.techcultivation.deploy.paymentProcessorHbci = {
    dockerImage.enable = lib.mkEnableOption "sangha hbci payment processor docker image";
  };

  config = lib.mkIf cfg.dockerImage.enable {
    generated.dockerImages.sanghaPaymentProcessorHbci =
      pkgs.helpers.createStandaloneDockerImage {
        name = "sangha-payment-processor-hbci";
        tag = "deployment";
        description = "TODO";

        contents = [
          pkgs.payment-processor-hbci
        ];

        config = {
          User = "payment-processor-hbci";
          Cmd = [ "/bin/hbci" ];
        };
    };
  };
}
