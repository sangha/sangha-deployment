{ lib, pkgs, config, testing, ... }:
let

in {

  ## module options for queue deployment

  options.org.techcultivation.deploy.queue = {
    enable = lib.mkEnableOption "queue container";
    dockerImage.enable = lib.mkEnableOption "queue container docker image";
  };

  ## option implementation

  config =
    let
      nixosConfig = {
        # TODO
      };

      dockerConfig =
        let

          dockerImage =
            let
              # The erlang closure is giant by default;
              # rabbitmq doesn’t need most of it.
              # see https://github.com/rabbitmq/erlang-rpm
              # It still compiles some OTP libraries like wx,
              # that could be removed (~30–40M).
              rabbitmq_server-minimal = pkgs.rabbitmq_server.override {
                erlang = pkgs.erlang.override {
                  javacSupport = false;
                  odbcSupport = false;
                  wxSupport = false;
                  # prevent doc generation (>100M)
                  installTargets = ["install"];
                };
              };
              sh-minimal = pkgs.runCommand "sh-minimal" {} ''
                mkdir -p $out/bin
                cp ${pkgs.dash}/bin/dash $out/bin/sh
              '';

              userName = "rabbitmq";

              image = pkgs.helpers.createStandaloneDockerImage {
                name = "sangha-rabbitmq";
                tag = "deployment";
                description = "TODO";

                contents = [
                  rabbitmq_server-minimal
                  # only needed by the /bin/rabbitmq-env script,
                  # 11M that can probably be avoided
                  pkgs.coreutils
                  pkgs.gnused
                  # erlang shells out in a library function m(
                  sh-minimal
                ];

                config = {
                  User = userName;
                  Cmd = [ "/bin/rabbitmq-server" ];
                  Env = [
                    # logs to stdout
                    "RABBITMQ_LOGS=-"
                    "RABBITMQ_SASL_LOGS=-"
                    "HOME=/var/lib/rabbitmq"
                  ];
                  ExposedPorts."${toString 5672}/tcp" = {};
                };

                rootSetupScript = ''
                  # rabbit setup
                  mkdir -p /var/lib/rabbitmq/mnesia
                  chown --recursive ${userName}:${userName} /var/lib/rabbitmq

                  # add a symlink to the .erlang.cookie in /root
                  # so we can "docker exec rabbitmqctl ..." without gosu
                  # mkdir /root
                  # ln -sf /var/lib/rabbitmq/.erlang.cookie /root/
                '';
              };
            in image;

        in {
          generated.dockerImages.sanghaRabbitmq = dockerImage;
        };

    in
      let
        cfg = config.org.techcultivation.deploy.queue;
      in
        lib.mkMerge [
          (lib.mkIf cfg.enable nixosConfig)
          (lib.mkIf cfg.dockerImage.enable dockerConfig)
        ];


}
