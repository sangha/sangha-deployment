{ nixpkgsPath ? import ./nixpkgs.nix }:
let
  lib = import "${toString nixpkgsPath}/lib";
  nixosFunction = import "${toString nixpkgsPath}/nixos";

  # the base config
  baseConfig = { ... }: {
    # import all module definitions
      imports = [
        ./modules/docker-images.nix
        ./postgres.nix
        ./rabbitmq.nix
      ./sangha-api.nix
      ./payment-processor-hbci.nix
      ];

      config = {
      # enable docker image generation
        org.techcultivation.deploy.database.dockerImage.enable = true;
        org.techcultivation.deploy.queue.dockerImage.enable = true;
      org.techcultivation.deploy.api.dockerImage.enable = true;
      org.techcultivation.deploy.paymentProcessorHbci.dockerImage.enable = true;

        # our own packages
        nixpkgs.overlays = [(self: _: {
          sangha = self.callPackage ../packages/sangha {};
        payment-processor-hbci = self.callPackage ../packages/payment-processor-hbci {};

        # lots of small and big helper functions
        helpers = import ./helpers self.helpers self;
        })];
      };
    };

  # final configs for dev & deployment
  deployConfig = baseConfig;
  devConfig = {
    imports = [ baseConfig ];
    config = {
      org.techcultivation.deploy.database.dockerImage.insertMockData = true;
    };
  };

  # eval a config
  doEval = config: nixosFunction {
    configuration = config;
  };

  # evaluated configs
  evalDeploy = doEval deployConfig;
  evalDev = doEval devConfig;

  inherit (evalDeploy.config._module.args) pkgs;

  # TODO tmp
  sanghaPostgresTemplate = evalDev.config.passthru;

  # make docker image suitable for download from Hydra
  buildHydraDockerImage = image: pkgs.runCommand image.imageName {
      inherit image;
      passthru.meta = image.meta;
    }
    # produces an output link for hydra
    ''
      mkdir -p "$out/nix-support"
      echo "file docker-image $image" \
        > "$out/nix-support/hydra-build-products"
    '';


in {
  pkgs = {
    inherit (pkgs) sangha payment-processor-hbci;
    inherit sanghaPostgresTemplate;
   };
  downloads.dockerImages.sangha = lib.mapAttrs (lib.const buildHydraDockerImage) {
    postgres = evalDeploy.config.generated.dockerImages.sanghaPostgres;
    postgresDev = evalDev.config.generated.dockerImages.sanghaPostgres;
    rabbitmq = evalDeploy.config.generated.dockerImages.sanghaRabbitmq;
    api = evalDeploy.config.generated.dockerImages.sanghaApi;
    paymentProcessorHbci = evalDeploy.config.generated.dockerImages.sanghaPaymentProcessorHbci;
  };
  # FIXME: evalDev is implicitely needed, otherwise the test
  # doesn’t get the mock database.
  # This happens because the is in the same file as the postgres
  # module definition and therefore inherits its options.
  tests = evalDev.config.tests;
}
