# a version of nixpkgs that has a few additional patches
# to enable the nix definitions in this folder.
# Plus, it’s pinned to a well-known working state.
let
  domain = "gitlab.com/";
  owner = "Profpatsch";
  repo = "nixpkgs-techcultivation";
  rev = "a04fa4343aaab8ca0b5f6382522b32d18dc60504";
  sha256 = "0s3wf4wb2rg4j7f326bi1iaxgilh5i1qp0hgq58mmyv2sp14jcmv";

in builtins.fetchTarball {
  # taken from the nixpkgs definition of `fetchFromGitLab`
  url = "https://${domain}/api/v4/projects/${owner}%2F${repo}/repository/archive.tar.gz?sha=${rev}";
  inherit sha256;
}
