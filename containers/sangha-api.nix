{ lib, pkgs, config, ... }:
let
  cfg = config.org.techcultivation.deploy.api;
in {

  options.org.techcultivation.deploy.api = {
    dockerImage.enable = lib.mkEnableOption "sangha API docker image";
  };

  config = lib.mkIf cfg.dockerImage.enable {
    generated.dockerImages.sanghaApi =
      pkgs.helpers.createStandaloneDockerImage {
        name = "sangha-api";
        tag = "deployment";
        description = "TODO";

        contents = [
          pkgs.sangha
        ];

        config = {
          User = "sangha-api";
          Cmd = [ "/bin/sangha" ];
          ExposedPorts."${toString 9991}/tcp" = {};
        };
    };
  };
}
