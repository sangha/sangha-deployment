{ lib, pkgs, config, testing, ... }:
let
  cfg = config.org.techcultivation.deploy.database;

  # set database connection values
  connectionData = {
    Host = "";
    Port = 5432;
    DbName = "sangha";
    User = "sanghauser";
    Password = "sanghapass";
    SslMode = "disable";
  };

  # locale used by the database
  sanghaDatabaseLocale = "en_US.UTF-8";

  # TODO: use pgtune(1) for full deployment setup
  # TODO: max_connections & shared_buffers
  # set by initdb, should be modifiable
  # see https://www.postgresql.org/docs/9.1/static/runtime-config-resource.html
  # and https://www.postgresql.org/docs/9.1/static/runtime-config-connection.html

  sanghaPostgresConfig =
    let l = sanghaDatabaseLocale;
    in {
      listen_addresses = "*";
      max_connections = 100;
      shared_buffers = "128MB";
      log_timezone = "UTC";
      timezone = "UTC";
      lc_messages = l;
      lc_monetary = l;
      lc_numeric = l;
      lc_time = l;
    };

  # creates a postgres database folder, complete with sangha
  # databases and mock data
  sanghaInitDatabase =
    let
      # sangha config, using database connection values from above
      sanghaConfig = pkgs.runCommand "sangha.conf" {} ''
        # copy sangha configuration with correct connection data
        ${lib.getBin pkgs.jq}/bin/jq '.Connections.PostgreSQLConnection *= ${
                lib.generators.toJSON {} connectionData
              }' \
          ${pkgs.sangha.src}/config.example.json \
          > $out
      '';

      # sangha db SQL initialization code
      sanghaCreateDb = with connectionData;
        pkgs.writeText "sangha-create-db.sql" ''
          CREATE DATABASE ${DbName};
          CREATE USER ${User} WITH PASSWORD '${Password}';
          GRANT ALL PRIVILEGES ON DATABASE ${DbName} TO ${User};
        '';

      template = pkgs.helpers.postgresTemplate {
        name = "sangha";
        locale = sanghaDatabaseLocale;
        postgresqlConf = sanghaPostgresConfig;
        initScript = ''
          # init database
          psql --file=${sanghaCreateDb} --username=postgres

          # init database tables
          # TODO: use command line flag of sangha (buggy)
          cp ${sanghaConfig} ./config.json
          ${lib.getBin pkgs.sangha}/bin/sangha database init

          # insert mock data
          psql --file=${pkgs.sangha.src}/mocks/projects.sql --username=postgres sangha
        '';
      };

    in template.overrideAttrs (_: {
        # pass through the sangha configfile used
        passthru = { inherit sanghaConfig; };
        meta.description = ''
          Postgres database for sangha, completely initialized
          and with mock data.
        '';
      });

in {

  ## module options for database deployment

  # TODO: might need to change the namespace name
  options.org.techcultivation.deploy.database = {
    enable = lib.mkEnableOption "database container";
    dockerImage = {
      enable = lib.mkEnableOption "database container docker image";
      insertMockData = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = ''
          Whether to add the mock data (sets tag to "mock").
        '';
  };
    };
  };


  ## option implementation

  config =
    let
      # configuration for usage on NixOS / with nspawn containers
      nixosConfig = {
        # TODO
      };

      # configuration for generating a docker image
      # TODO: this is a bit long, refactor!
      dockerConfig =
        let
          dockerCmd = "/bin/postgres";
          dbDir = "/database";

          dockerImage =
            let
              userName = "postgres";

              isDev = cfg.dockerImage.insertMockData;

              image = pkgs.helpers.createStandaloneDockerImage {
                name = "sangha-postgres";
                tag =
                  if isDev
                  then "dev"
                  else "deployment";
                description = ''
                  Docker image containing a minimal postgres closure
                  and an initialized sangha database template in a volume.
                '';

                # contents of the docker image
                contents = [
                  # the complete postgresql closure with all deps
                  pkgs.postgresql
                  # needed locales (localdb)
                  (pkgs.helpers.prependPrefix "/usr"
                    (pkgs.helpers.genUtf8Localedb sanghaDatabaseLocale))
                ];

                # docker config (see docker image spec)
                config = {
                  User = userName;
                  Cmd = [ dockerCmd ];
                  ExposedPorts."${toString connectionData.Port}/tcp" = {};
                }
                # only if mock data should be inserted
                // (if cfg.dockerImage.insertMockData then {
                  Env =
                    lib.optional
                      cfg.dockerImage.insertMockData
                      "PGDATA=${dbDir}";
                  # we create a volume so the db is persistent over restarts
                  Volumes."${dbDir}" = {};
                } else {});

                rootSetupScript =
                  # setup for mock data
                  lib.optionalString cfg.dockerImage.insertMockData ''
                  cp -r "${sanghaInitDatabase}" "./${dbDir}"
                  chmod --recursive u=rwX,go= "./${dbDir}"
                  chown --recursive ${userName}:${userName} "./${dbDir}"
                '';
              };
            in image;

          dockerTest = testing.makeTest {
            name = "sangha-postgres-docker-integration-test";
            nodes = {
              server = {...}: {
                virtualisation.docker.enable = true;
                networking.firewall.enable = false;
                # TODO: somehow enable here
                # org.techcultivation.deploy.database.dockerImage.insertMockData = true;
              };
              client = { };
            };

            testScript =
              let
                image = config.generated.dockerImages.sanghaPostgres;
                hba_file = let cd = connectionData; in pkgs.writeText "pg_hba.conf" ''
                  host ${cd.DbName} ${cd.User} 0.0.0.0/0 trust
                '';
              in ''
                startAll;
                $server->waitForUnit("docker.service");
                $server->succeed("docker load -i ${image}");
                $server->succeed("docker run --detach=true --network=host "
                               . "--volume=${hba_file}:/conf/pg_hba.conf "
                               . "${image.imageName}:${image.imageTag} "
                                 . "${dockerCmd} "
                                 . "-D ${dbDir} "
                                 . "-c'data_directory=${dbDir}' "
                                 . "-c'hba_file=/conf/pg_hba.conf' ");
                $server->waitForOpenPortTimeout(${toString connectionData.Port}, 10);
                $client->succeed('systemd-cat ${pkgs.postgresql}/bin/psql --host=server '
                               . '--username=${connectionData.User} '
                               . '--dbname=${connectionData.DbName} '
                               . '--command=\'\dt\' ');
              '';
          };

        in {
          generated.dockerImages.sanghaPostgres = dockerImage;
          tests.dockerImages.sanghaPostgres = dockerTest;
        };

      in
        lib.mkMerge [
          (lib.mkIf cfg.enable nixosConfig)
        (lib.mkIf cfg.dockerImage.enable dockerConfig)
          ({
            # TODO temporary
            passthru = sanghaInitDatabase;
          })
        ];

}
