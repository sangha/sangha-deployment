{ lib, stdenv, buildGoPackage, fetchgit, aqbanking, gwenhywfar }:
let
  # gwenhywfar drags in gtk & qt by default …
  gwenhywfarSmall = gwenhywfar.override {
    guiSupport = false;
  };
  aqbankingSmall = aqbanking.override {
    gwenhywfar = gwenhywfarSmall;
  };

in buildGoPackage rec {
  name = "payment-processors-unstable-${version}";
  version = "2018-03-12";
  rev = "7f0adc67638e9b369519d85b9a59c776bd89ccc4";

  subPackages = [ "./hbci" ];

  goPackagePath = "gitlab.com/sangha/payment-processors";

  src = fetchgit {
    inherit rev;
    url = "https://gitlab.com/sangha/payment-processors.git";
    sha256 = "149nqp1299llhb1xy0ljz6r3h77gxa9jyqzhx1hgkrghjlij1hzk";
  };

  goDeps = ./deps.nix;

  preBuild = with lib; ''
    export CGO_CFLAGS="-I${getDev aqbankingSmall}/include/aqbanking5 -I${getDev gwenhywfarSmall}/include/gwenhywfar4"
    export CGO_LDFLAGS="-L${getDev aqbankingSmall}/lib -L${getDev gwenhywfarSmall}/lib"
  '';

  # TODO: add metadata https://nixos.org/nixpkgs/manual/#sec-standard-meta-attributes
  meta = {
  };
}
