{ stdenv, buildGoPackage, fetchgit }:

buildGoPackage rec {
  name = "sangha-unstable-${version}";
  version = "2017-11-04";
  rev = "cbcf29b527008e1f1403c00d9edd61f4903553cf";

  goPackagePath = "gitlab.com/sangha/sangha";

  subPackages = [ "." ];

  src = fetchgit {
    inherit rev;
    url = "https://gitlab.com/sangha/sangha.git";
    sha256 = "1nfnlpp7bdjhhws0swlg4lxh919vd3yd1nyag8d2mq8cm9ws6yqx";
  };

  goDeps = ./deps.nix;

  # TODO: add metadata https://nixos.org/nixpkgs/manual/#sec-standard-meta-attributes
  meta = {
  };
}
