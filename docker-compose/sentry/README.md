# Sentry compose

```bash
# generate secret key (change environment in docker-compose)
docker-compose exec sentry sentry config generate-secret-key

# start with docker compose
docker-compose up

# generate config and initial user
docker-compose exec sentry sentry upgrade
```
