# Compose files

## requirements
  * docker and docker-compose `apt install docker.io docker-compose`

## usage
  * cd to stack you want to start `cd sangha`
  * copy `example.env` to `.env` and change according to your settings
  * start containers `docker-compose up -d`

## updating
  * when modified `docker-compose.yml` or `.env` update via `docker-compose up -d`
  * update containers with `docker-compose pull && docker-compose up -d`
