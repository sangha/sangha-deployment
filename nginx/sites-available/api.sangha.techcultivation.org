server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        ssl_certificate /etc/letsencrypt/live/api.sangha.techcultivation.org/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/api.sangha.techcultivation.org/privkey.pem;
        include /etc/nginx/snippets/ssl-configuration.conf;
        include /etc/nginx/snippets/sanitize-logs-per-server.conf;

        server_name api.sangha.techcultivation.org;

        location = / {
 			rewrite ^ https://techcultivation.org/ redirect;
         }

    	location /v1/ {
	    	proxy_pass http://10.0.3.206:9991/v1/;
	    }  
}
