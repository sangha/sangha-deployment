server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        ssl_certificate /etc/letsencrypt/live/sandbox.api.sangha.techcultivation.org/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/sandbox.api.sangha.techcultivation.org/privkey.pem;
        include /etc/nginx/snippets/ssl-configuration.conf;
        include /etc/nginx/snippets/sanitize-logs-per-server.conf;

        server_name sandbox.api.sangha.techcultivation.org;

	location = / {
		rewrite ^ https://techcultivation.org/ redirect;
	}

        location /v1/ {
                proxy_pass http://10.0.3.71:9991/v1/;
        }
}
