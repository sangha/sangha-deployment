package storage

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

type DB struct {
	conn *sql.DB
}

func Open(sqliteDb string) (*DB, error) {
	conn, err := sql.Open("sqlite3", sqliteDb)
	if err != nil {
		return nil, err
	}
	db := &DB{conn: conn}
	err = db.init()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func (db *DB) init() error {
	// TODO INIT DB
	return nil
}

func (db *DB) Close() error {
	return db.conn.Close()
}
