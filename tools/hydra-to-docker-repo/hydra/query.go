// Package hydra interfaces with the hydra JSON API.
package hydra

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"time"
)

// RETRIES hydra reqeusts returning status 500 this many times.
var RETRIES = 3

// TIMEOUTs http requests after this amount of seconds.
var TIMEOUT = 10 * time.Second

// Hydra exposes interaction with a hydra instance.
type Hydra struct {
	baseUrl url.URL
	client  *http.Client
}

// New initializes a Hydra connection.
func New(baseUrl url.URL) *Hydra {
	return &Hydra{
		baseUrl: baseUrl,
		client:  &http.Client{Timeout: TIMEOUT},
	}
}

// JobsetRef references a hydra Jobset.
type JobsetRef struct {
	Project string
	Name    string
}

// JobRef references a hydra Job.
type JobRef struct {
	Jobset JobsetRef
	Name   string
}

// Endpoint of hydra that can be queried.
type Endpoint string

// Eval JSON output of hydra (partial).
type EvalResponse struct {
	Evals []struct {
		// unique id of that eval
		Id           int
		HasNewBuilds int
		// list of build ids
		Builds []int
	}
}

// Eval the result of a Hydra evaluation.
type Eval struct {
	HasNewBuilds bool
	Builds       []int
}

type Evals map[int]Eval

func intToBool(fieldName string, i int) (bool, error) {
	switch i {
	case 0:
		return false, nil
	case 1:
		return true, nil
	default:
		return false, fmt.Errorf("intToBool: %d is not a valid boolean, for %s", i, fieldName)
	}
}

func responseToEvals(resp *EvalResponse) (*Evals, error) {
	evals := make(Evals, len(resp.Evals))
	for _, eval := range resp.Evals {
		hnb, err := intToBool(fmt.Sprintf("eval %d, HasNewBuilds", eval.Id), eval.HasNewBuilds)
		if err != nil {
			return nil, err
		}
		evals[eval.Id] = Eval{
			HasNewBuilds: hnb,
			Builds:       eval.Builds,
		}
	}
	return &evals, nil
}

// BuildOutput is a nix derivation output, like "doc".
type BuildOutput struct {
	// nix storepath of output
	Path string
}

// BuildProduct is a Hydra build product
// (in the magic `nix-support/hydra-build-products` file of an output).
type BuildProduct struct {
	// name of build output
	Name string
	// nix storepath of output, can be used to associate to buildoutput
	Path string
	// sha256hash of output file, null if no output file
	Sha256Hash string
}

// Build JSON output of hydra (partial).
type BuildResponse struct {
	// unique id of that build
	Id int
	// buildstatus, 0 if successful
	BuildStatus int
	// finished, 1 if finished
	Finished int
	// nix derivation `outputs` field, keys "out", "man" …
	BuildOutputs map[string]BuildOutput
	// hydra outputs of build
	BuildProducts map[string]BuildProduct
	// hydra job attribute (nix attribute in hydra attrset, e.g. "pkgs.sangha")
	Job string
}

// Build is a Hydra Build.
type Build struct {
	Id              int
	BuildSuccessful bool
	BuildOutputs    map[string]BuildOutput
	BuildProducts   map[string]BuildProduct
	Job             string
}

func responseToBuild(resp *BuildResponse) (*Build, error) {
	finished, err := intToBool(fmt.Sprintf("build %d", resp.Id), resp.Finished)
	if err != nil {
		return nil, err
	}
	buildSuccessful := false
	if finished && (resp.BuildStatus == 0) {
		buildSuccessful = true
	}
	return &Build{
		Id:              resp.Id,
		BuildSuccessful: buildSuccessful,
		BuildOutputs:    resp.BuildOutputs,
		BuildProducts:   resp.BuildProducts,
	}, nil
}

// MkEndpoint creates an Endpoint, sanity checking it.
func MkEndpoint(rawEndpoint string) (Endpoint, error) {
	url, err := url.Parse(rawEndpoint)
	if err != nil {
		return "", err
	}
	if url.IsAbs() {
		return "", fmt.Errorf("hydraFetch: Endpoint URL must not be absolute (%v)", rawEndpoint)
	}
	return Endpoint(url.Path), err
}

// fetchJson queries a hydra endpoint and retries if necessary.
func (h *Hydra) fetchJson(e Endpoint) (io.ReadCloser, error) {
	url := h.baseUrl
	url.Path = path.Join(h.baseUrl.Path, string(e))

	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")

	// Hydra is buggy like that, so we retry for a bit
	retryOn500 := func(req *http.Request) (*http.Response, error) {
		for i := 0; i < RETRIES; i++ {
			resp, err := h.client.Do(req)
			if err != nil {
				return nil, err
			}
			switch resp.StatusCode {
			case http.StatusInternalServerError:
				continue
			default:
				return resp, nil
			}

		}
		return nil, fmt.Errorf("hydraFetch: More than %d retries for %s", RETRIES, url.String())
	}

	resp, err := retryOn500(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {
		return resp.Body, nil
	}
	return nil, fmt.Errorf("hydraFetch: %s returned %s", url.String(), resp.Status)

}

func (h *Hydra) FetchBuildFromEndpoint(endp Endpoint) (*Build, error) {
	response, err := h.fetchJson(endp)
	if err != nil {
		return nil, err
	}
	defer response.Close()

	var result BuildResponse
	err = json.NewDecoder(response).Decode(&result)

	if err != nil {
		return nil, err
	}
	return responseToBuild(&result)
}

// FetchBuild by Hydra build id.
func (h *Hydra) FetchBuild(buildId int) (*Build, error) {
	endp, _ := MkEndpoint("build/" + strconv.Itoa(buildId))
	return h.FetchBuildFromEndpoint(endp)
}

// FetchLatestFinishedBuild for a Hydra job.
func (h *Hydra) FetchLatestFinishedBuild(job JobRef) (*Build, error) {
	endp, _ := MkEndpoint(fmt.Sprintf("job/%s/%s/%s/latest-finished",
		job.Jobset.Project, job.Jobset.Name, job.Name))
	return h.FetchBuildFromEndpoint(endp)
}

// FetchEvals for a Hydra jobset.
func (h *Hydra) FetchEvals(jobset JobsetRef) (*Evals, error) {
	endp, _ := MkEndpoint(
		fmt.Sprintf("jobset/%s/%s/evals", jobset.Project, jobset.Name))
	response, err := h.fetchJson(endp)
	if err != nil {
		return nil, err
	}
	defer response.Close()

	var result EvalResponse
	err = json.NewDecoder(response).Decode(&result)

	if err != nil {
		return nil, err
	}
	return responseToEvals(&result)
}
