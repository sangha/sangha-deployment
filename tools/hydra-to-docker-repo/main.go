package main

import (
	"fmt"
	"gitlab.com/sangha-deployment/tools/hydra-to-docker-repo/hydra"
	// "log"
	"net/url"
)

func main() {
	base, _ := url.Parse(`https://headcounter.org/hydra`)
	h := hydra.New(*base)

	build, err := h.FetchBuild(2095071)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%+v\n", build)

	eval, err := h.FetchEvals(hydra.JobsetRef{
		Project: "techcultivation",
		Name:    "sangha-deployment"})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%+v\n", eval)

	build, err = h.FetchLatestFinishedBuild(hydra.JobRef{
		Name: "downloads.dockerImages.sangha.postgres",
		Jobset: hydra.JobsetRef{
			Project: "techcultivation",
			Name:    "sangha-deployment",
		},
	})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%+v\n", build)
}
